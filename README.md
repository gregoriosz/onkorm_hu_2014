# Önkorm_HU_2014

A 2014-es önkormányzati választások eredményeinek letöltése **R**-ben a valasztas.hu-ról.

Polgármester, főpolgármester, és egyéni önkormányzati képviselők választásának eredményei szavazókörönként.